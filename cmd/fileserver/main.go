/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"

	assetfs "github.com/elazarl/go-bindata-assetfs"
)

var (
	buildInfo  string
	buildStamp string
	gitHash    string
	version    string
	rootDir    = "./"
)

func main() {
	if buildInfo != "" {
		parts := strings.Split(buildInfo, "|")
		if len(parts) >= 3 {
			buildStamp = parts[0]
			gitHash = parts[1]
			version = parts[2]
		}
	}

	server := &http.Server{}
	http.Handle("/", addCacheControl(http.StripPrefix("/", http.FileServer(&assetfs.AssetFS{
		Asset:     Asset,
		AssetDir:  AssetDir,
		AssetInfo: AssetInfo,
		Prefix:    "dist/",
	}))))

	// https://gist.github.com/xcsrz/538e291d12be6ee9a8c7
	listener, err := net.Listen("tcp", "localhost:5000")
	if err != nil {
		panic(err)
	}
	log.Printf("listening on http://%s", listener.Addr().String())

	if err = open("http://" + listener.Addr().String() + "/index.html"); err != nil {
		fmt.Fprintln(os.Stderr, "unable to open default browser", err)
		os.Exit(1)
	}

	if err = server.Serve(listener); err != http.ErrServerClosed {
		panic(err)
	}
}

func addCacheControl(h http.Handler) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		res.Header().Set("Cache-Control", "no-store")
		h.ServeHTTP(res, req)
	}
}

// http://stackoverflow.com/a/39324149/96233
func open(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}
