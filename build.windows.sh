#!/bin/sh

cd "${0%/*}"

if go-bindata -pkg main -prefix $(dirname "$1") "$1"
then
    mv bindata.go cmd/fileserver
    buildInfo="`date -u '+%Y-%m-%dT%TZ'`|`git describe --always --long`|`git tag | tail -1`"
    GOOS=windows GOARCH=amd64 go build -ldflags "-X main.buildInfo=${buildInfo} -s -w" ./cmd/...
fi
